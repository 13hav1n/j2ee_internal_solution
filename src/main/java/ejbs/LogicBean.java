package ejbs;

import entities.Billdetails;
import entities.Billmaster;
import entities.Customer;
import entities.Product;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class LogicBean implements LogicBeanLocal {
    
    @PersistenceContext(unitName = "main_pu")
    EntityManager em;
    
    @Override
    public Collection<Customer> getAllCustomer() {
        return em.createNamedQuery("Customer.findAll").getResultList();
    }
    
    @Override
    public Customer getCustomerById(Integer id) {
        Customer customer = (Customer) em.find(Customer.class, id);
        return customer;
    }
    
    @Override
    public Collection<Product> getAllProducts() {
        return em.createNamedQuery("Product.findAll").getResultList();
    }
    
    @Override
    public Product getProductById(Integer id) {
        Product product = (Product) em.find(Product.class, id);
        return product;
    }
    
    @Override
    public void insertIntoBillMaster(Billmaster billMaster) {
        if (billMaster != null) {
            em.persist(billMaster);
        }
    }
    
    @Override
    public void insertIntoBillDetails(Billdetails billDetails) {
        if (billDetails != null) {
            em.persist(billDetails);
        }
    }

    @Override
    public Collection<Billmaster> getBillmasterList() {
        return em.createNamedQuery("Billmaster.findAll").getResultList();
    }

    @Override
    public Collection<Billdetails> getBilldetailsList() {
        return em.createNamedQuery("Billdetails.findAll").getResultList();
    }

    @Override
    public Billmaster getBillMasterById(Integer billId) {
        Billmaster billMaster = (Billmaster) em.find(Billmaster.class, billId);
        return billMaster;
    }

    @Override
    public Collection<Billdetails> getDetailsFromBillmaster(Billmaster billMaster) {
        Collection<Billdetails> bds = em.createNamedQuery("Billdetails.findByBillId").setParameter("billId", billMaster).getResultList();
        return bds;
    }
}
