
package ejbs;

import entities.Billdetails;
import entities.Billmaster;
import entities.Customer;
import entities.Product;
import java.util.Collection;
import javax.ejb.Local;


@Local
public interface LogicBeanLocal {
    Collection<Customer> getAllCustomer();
    Customer getCustomerById(Integer id);
    
    Collection<Product> getAllProducts();
    Product getProductById(Integer id);
    
    Collection<Billmaster> getBillmasterList();
    Billmaster getBillMasterById(Integer billId);
    void insertIntoBillMaster(Billmaster billMaster);
    
    Collection<Billdetails> getBilldetailsList();
    void insertIntoBillDetails(Billdetails billDetails);
    Collection<Billdetails> getDetailsFromBillmaster(Billmaster billMaster);
}
