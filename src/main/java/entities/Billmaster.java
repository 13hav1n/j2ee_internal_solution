/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Lenovo
 */
@Entity
@Table(name = "billmaster")
@NamedQueries({
    @NamedQuery(name = "Billmaster.findAll", query = "SELECT b FROM Billmaster b"),
    @NamedQuery(name = "Billmaster.findById", query = "SELECT b FROM Billmaster b WHERE b.id = :id"),
    @NamedQuery(name = "Billmaster.findByGrossTotal", query = "SELECT b FROM Billmaster b WHERE b.grossTotal = :grossTotal")})
public class Billmaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "grossTotal")
    private Integer grossTotal;
    @JoinColumn(name = "customerId", referencedColumnName = "id")
    @ManyToOne
    private Customer customerId;
    @OneToMany(mappedBy = "billId")
    private Collection<Billdetails> billdetailsCollection;

    public Billmaster() {
    }

    public Billmaster(Integer id) {
        this.id = id;
    }

    public Billmaster(Customer customer, Integer grossTotal){
        this.grossTotal = grossTotal;
        this.customerId = customer;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGrossTotal() {
        return grossTotal;
    }

    public void setGrossTotal(Integer grossTotal) {
        this.grossTotal = grossTotal;
    }

    public Customer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    @JsonbTransient
    public Collection<Billdetails> getBilldetailsCollection() {
        return billdetailsCollection;
    }

    public void setBilldetailsCollection(Collection<Billdetails> billdetailsCollection) {
        this.billdetailsCollection = billdetailsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Billmaster)) {
            return false;
        }
        Billmaster other = (Billmaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Billmaster[ id=" + id + " ]";
    }
    
}
