/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Lenovo
 */
@Entity
@Table(name = "billdetails")
@NamedQueries({
    @NamedQuery(name = "Billdetails.findAll", query = "SELECT b FROM Billdetails b"),
    @NamedQuery(name = "Billdetails.findById", query = "SELECT b FROM Billdetails b WHERE b.id = :id"),
    @NamedQuery(name = "Billdetails.findByQuantity", query = "SELECT b FROM Billdetails b WHERE b.quantity = :quantity"),
    @NamedQuery(name = "Billdetails.findByTotal", query = "SELECT b FROM Billdetails b WHERE b.total = :total"),
    @NamedQuery(name = "Billdetails.findByBillId", query = "SELECT b FROM Billdetails b WHERE b.billId = :billId")})
public class Billdetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "total")
    private Integer total;
    @JoinColumn(name = "billId", referencedColumnName = "id")
    @ManyToOne
    private Billmaster billId;
    @JoinColumn(name = "productId", referencedColumnName = "id")
    @ManyToOne
    private Product productId;

    public Billdetails() {
    }

    public Billdetails(Integer quantity, Billmaster billMaster, Product product){
        this.quantity = quantity;
        this.productId = product;
        this.billId = billMaster;
    }
    
    public Billdetails(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Billmaster getBillId() {
        return billId;
    }

    public void setBillId(Billmaster billId) {
        this.billId = billId;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Billdetails)) {
            return false;
        }
        Billdetails other = (Billdetails) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Billdetails[ id=" + id + " ]";
    }
    
}
