/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package cdis;

import ejbs.LogicBeanLocal;
import entities.Billdetails;
import entities.Billmaster;
import entities.Customer;
import entities.Product;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

@Named(value = "custCdi")
@ApplicationScoped
public class CustomerCDI {

    @EJB
    LogicBeanLocal lbl;

    Collection<Product> store;
    Customer loggedInCustomer;
    ArrayList<Product> kart;
    ArrayList<Integer> quantitiesPerProduct;

    Billmaster bm;

    Collection<Billdetails> billDetails;
    int quantities;

    public CustomerCDI() {
        store = new ArrayList<Product>();
        loggedInCustomer = new Customer();
        kart = new ArrayList<Product>();
        quantitiesPerProduct = new ArrayList<>();
        billDetails = new ArrayList<>();
        bm = new Billmaster();
        quantities = 0;
    }

    public int getQuantities() {
        return quantities;
    }

    public void setQuantities(int quantities) {
        System.out.println(quantities);
        if (quantities != 0) {
            quantitiesPerProduct.add(quantities);
            this.quantities = quantities;
        }
    }

    public Collection<Product> getStore() {
        return lbl.getAllProducts();
    }

    public void setStore(Collection<Product> store) {
        this.store = store;
    }

    public Customer getLoggedInCustomer() {
        return loggedInCustomer;
    }

    public void setLoggedInCustomer(int loggedInCustomer) {
        this.loggedInCustomer = (Customer) lbl.getCustomerById(loggedInCustomer);
    }

    public Collection<Product> getKart() {
        return kart;
    }

    public void setKart(ArrayList<Product> kart) {
        if (!kart.isEmpty()) {
            this.kart.add(kart.get(0));
        }
    }

    public Collection<Billmaster> getBillmasterList() {
        return lbl.getBillmasterList();
    }

    public Collection<Billdetails> getBillDetails() {
        return billDetails;
    }

    public void setBillDetails(Collection<Billdetails> billDetails) {
        this.billDetails = billDetails;
    }

    public String getCurrentDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        String dateString = String.valueOf(dtf.format(now));
        return dateString;
    }

    public Billmaster getBm() {
        return bm;
    }

    public String doLogin() {
        loggedInCustomer = (Customer) lbl.getCustomerById(loggedInCustomer.getId());

        if (loggedInCustomer != null) {
            return "store.jsf";
        }
        return "login.jsf";
    }

    public String doPlaceOrder() {
        int grossTotal = 0;
        int discountRate;
        int total;
        for (int i = 0; i < kart.size(); i++) {
            discountRate = kart.get(i).getRate() * kart.get(i).getDiscount() / 100;
            grossTotal += (kart.get(i).getRate() - discountRate) * quantitiesPerProduct.get(i);
        }
        this.bm = new Billmaster(loggedInCustomer, grossTotal);
        lbl.insertIntoBillMaster(bm);

        Billdetails bd = new Billdetails();
        for (int i = 0; i < kart.size(); i++) {
            bd.setBillId(bm);
            bd.setQuantity(quantitiesPerProduct.get(i));
            bd.setProductId(kart.get(i));
            discountRate = kart.get(i).getRate() * kart.get(i).getDiscount() / 100;
            total = (kart.get(i).getRate() - discountRate) * quantitiesPerProduct.get(i);
            bd.setTotal(total);
            lbl.insertIntoBillDetails(bd);
            billDetails.add(bd);
        }
        if (grossTotal > 0) {
            return "orders.jsf";
        }
        return "store.jsf";
    }

    public String doOrderDetails(Integer BillId) {
        this.bm = lbl.getBillMasterById(BillId);
        this.billDetails = lbl.getDetailsFromBillmaster(bm);
        return "billinfo.jsf";
    }

    public String redirect(String string) {
        kart = new ArrayList<Product>();
        quantitiesPerProduct = new ArrayList<>();
        quantities = 0;
        return string;
    }

}
